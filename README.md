# MCEverywhere

## Need to install applications

1.	Microsoft Visual Studio
2.	Microsoft Visual Studio Code
3.	TortoiseGit
4.	.NET 3.1, 2.1, 4.8 SDKs
5.	Yarn 1.22.5
6.	Node 12.16.3
7.	SQL Server Management Studio

## Setting up VM environment
When setting up the VM, set Virtual switch to Internal so you will be able to access the files of your VM from your desktop. 

<img src="src/MCEverywhere/wwwroot/doc/screenshot1.png" width="50%" /><img src="src/MCEverywhere/wwwroot/doc/screenshot2.png" width="50%" />

In my case, I wasn’t able to access the files of the VM even after doing this so I shared c:/Maintenance Connection folder and made it accessible to everyone. 
You need to test connecting to SQL Server on the VM from your desktop too.

## Setting up MCe system

When cloning the MCe, it’s better to use SSH key so you won’t encounter an issue when building the system later.

<img src="src/MCEverywhere/wwwroot/doc/screenshot3.png" width="50%" />

After making sure that everything’s working you can now set up the MCe to connect to database.  To connect you need to create **appsettings.\<computer name\>.json** file with the following code and save it under mceverywhere-e7\src\MCEverywhere folder.

```json
{
  "ConnectionStrings": {
    "mcINIFile": "\\\\IISMC8\\Maintenance Connection\\mc_iis\\mc.ini"
  }
}
```

**mcINIFile** is the location of mc.ini from your VM, **IISMC8** is the name of my VM. 

After this, you can now build the system. To build you need to run startMCe.bat and startMCe.UI.bat, there files located under mceverywhere-e7 folder.

## Issues encountered when setting up


##### 1. **Unable to remove directory “obj\Debug\netcoreapp3.1\Razor\”** 
<br/>
<img src="src/MCEverywhere/wwwroot/doc/screenshot4.png" width="70%" />
<br/><br/>

- To solve this, I deleted the entire obj folder under mceverywhere-e7\src\MCEverywhere.
<br/><br/>

##### 2. **DirectoryNotFoundException: Could not find a part of the path“C:\Users\jenel\source\aps\mceverywhere-e7\src\MCEverywhere\wwwroot\runtime\manifest.json”**
<br/><br/>
<img src="src/MCEverywhere/wwwroot/doc/screenshot5.png" width="60%" />
<br/><br/>

- To solve this, I checked out the branch Cam_LoginHubIntegration and switched back to master branch. The missing files remained even after switching back to master branch.
<br/><br/>

##### 3. **AuthenticationException: The remote certificate is invalid according to the validation procedure.**
<br/>
<img src="src/MCEverywhere/wwwroot/doc/screenshot6.png" width="60%" />
<br/><br/>

- To solve this, you need to run the following command to PowerShell console.

  ```
    dotnet dev-certs https -ep $env:USERPROFILE\.aspnet\https\aspnetapp.pfx -p crypticpassword
    dotnet dev-certs https --trust
  ``` 
<br/>

##### 4. **Unable to logged in even if the credentials are correct.** 
<br/>
<img src="src/MCEverywhere/wwwroot/doc/screenshot7.png" />
<br/><br/>

- To solve this, I deleted function calls udfDecryptString and udfEncryptString under Programmability / Functions / Scalar-valued Functions.
<br/><br/>
<img src="src/MCEverywhere/wwwroot/doc/screenshot8.png" height="500px" />
<br/><br/>

##### 5. **Able to logged in but got stuck after selecting database and had this console error.**
<br/>
<img src="src/MCEverywhere/wwwroot/doc/screenshot9.png" width="70%" />
<br/><br/>

- To solve this, I accessed the below url to upgrade the database. **https://localhost:44352/upgrades/ent**
<br/><br/>

## SQL To Create Translations files

Notes:
1. In Excel ensure the save type is "Unicode Text" otherwise data will be lost.
2. Don't forget to remove the final NULL row or it will crash the application

- For Languages: `*.langauges.txt`
```sql
SELECT        LanguageCode, LanguageName, fkeySelfAsParent, CAST(WrittenLtoR AS int) AS Expr1, CAST(WrittenInLines AS int) AS Expr2, ExternalCode + BrowserCodes AS Expr3, Comment
FROM            apsTranslationLanguages
```

- For Phrases: `*.phrases.txt`
```sql
SELECT        PhraseKey, fKeySystems
FROM            apsTranslationPhrases
```

-- For Translations `*.<lang>.tran.txt`
```sql
SELECT        fKeyTranslationPhrase, Translation
FROM            apsSystemTranslations
WHERE        (fKeyTranslationLanguage = 'en')
```
